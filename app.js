var id
var stages = $('#stages')
var loaded = localStorage.getItem('data')
var data
var notificationsEnabled
var timer
var timeLeft
var audio = {
  file: new Audio('./beep.wav'),
  ended: function () {
    setTimeout(() => {
      this.currentTime = 0
      this.play()
    }, 500)
  },
  play: function () {
    this.file.addEventListener('ended', this.ended)
    this.file.play()
  },
  stfu: function () {
    this.file.removeEventListener('ended', this.ended)
  }
}

var progressBar = new ProgressBar.Circle('#progress-circle', {
  strokeWidth: 2,
  trailWidth: 1,
  trailColor: '#fff',
  strokeColor: '#ddd' 
})

progressBar.set(1)

function Timer(callback, delay) {
  var timerId, start, remaining = delay;

  this.pause = function() {
    window.clearTimeout(timerId);
    remaining -= new Date() - start;
  };

  this.resume = function() {
    start = new Date();
    window.clearTimeout(timerId);
    timerId = window.setTimeout(callback, remaining);
  };

  this.resume();
}

var buttons = {
  addStage: $('#add-stage'),
  run: $('#run'),
  pause: $('#pause'),
  resume: $('#resume'),
  stop: $('#stop'),
  next: $('#next'),
  edit: $('#edit-btn')
}

var sections = {
  runDetails: $('#run-details'),
  runSection: $('#run-section'),
}

var save = function () {
  localStorage.setItem('data', JSON.stringify(data))
}

var deleteStage = function (del_id) {
  $(`#stage-${del_id}`).remove()
  delete data[del_id]
  save()
}

var changeName = function (event) {
  data[this].name = event.target.value
  save()
}

var changeLength = function (event) {
  data[this].length = parseInt(event.target.value)
  save()
}

var addStage = function (add_id, stage_data) {
  var stage = $('<li>')
    .attr('id', `stage-${add_id}`)

  var name = $('<input>')
    .attr('type', 'text')
    .attr('placeholder', 'Name')
    .on('change', changeName.bind(add_id))
    .val(stage_data.name)

  var length = $('<input>')
    .attr('type', 'number')
    .attr('placeholder', 'Length (seconds)')
    .on('change', changeLength.bind(add_id))
    .val(stage_data.length)

  var del_btn = $('<button>')
    .on('click', deleteStage.bind(null, add_id))

  del_btn.text('Delete')

  stage
    .append(name)
    .append(length)
    .append(del_btn)

  stages.append(stage)
}

var addNewStage = function (event) {
  let stage = {
    'name': '',
    'length': null
  }

  data[id] = stage
  addStage(id, stage)
  save()
  id += 1
}

var getNextIndex = function (indicies, index) {
  index += 1
  if (index === indicies.length) index = 0
  return index
}

var runStage = function (indicies, index, time) {
  window.index = index
  var stageIndex = indicies[index]
  var stage = data[stageIndex]
  var nextIndex = getNextIndex(indicies, index)
  var nextStage = data[indicies[nextIndex]]
  var delay = 1000
  var timeLeftString
  timeLeft = stage.length - time

  switch (timeLeft) {

  case -1:
    stage = nextStage
    index = nextIndex
    nextIndex = getNextIndex(indicies, index)
    nextStage = data[indicies[nextIndex]]
    timeLeft = stage.length
    time = 0
    delay = 0
    progressBar.stop()
    progressBar.set(1)

    if (notificationsEnabled) {
      new Notification('Timer over!', {
        body: `Time to ${stage.name}`
      })
    }
    setTimeout(pause, 0)
    audio.play()
    break

  default:
    time += 1
  }

  if (timeLeft > 60 * 60) {
    let hours = parseInt(timeLeft / 60 / 60)
    timeLeftString = `${hours} ${hours === 1 ? 'hour' : 'hours'} left`
  } else if (timeLeft > 60) {
    let minutes = parseInt(timeLeft / 60)
    timeLeftString = `${minutes} ${minutes === 1 ? 'minute' : 'minutes'} left`
  } else {
    timeLeftString = `${timeLeft} ${timeLeft === 1 ? 'second' : 'seconds'} left`
  }

  $('#current-stage').text(stage.name)
  $('#time-left').text(timeLeftString)
  $('#next-stage').text(nextStage.name)
  timer = new Timer(runStage.bind(null, indicies, index, time), delay)
}

var run = function () {
  let indicies = []

  for (let key in data) {
    indicies.push(parseInt(key))
  }
  indicies.sort()
  window.indicies = indicies
  runStage(indicies, 0, 0)
  progressBar.set(1)
  progressBar.animate(0, {
    duration: data[indicies[0]].length * 1000
  })
  sections.runDetails.show()
  buttons.stop.show()
  buttons.pause.show()
  buttons.run.hide()
  buttons.next.show()
}

var pause = function () {
  if (timer) {
    timer.pause()
  }
  progressBar.stop()
  buttons.pause.hide()
  buttons.resume.show()
}

var resume = function () {
  if (timer) {
    timer.resume()
  }
  progressBar.animate(0, {
    duration: timeLeft * 1000
  })
  audio.stfu()
  buttons.pause.show()
  buttons.resume.hide()
}

var stop = function () {
  if (timer) {
    timer.pause()
  }
  audio.stfu()
  progressBar.set(1)
  sections.runDetails.hide()
  buttons.pause.hide()
  buttons.stop.hide()
  buttons.next.hide()
  buttons.resume.hide()
  buttons.run.show()
}

var next = function () {
  let nextIndex = getNextIndex(window.indicies, window.index)

  if (timer) {
    timer.pause()
  }

  progressBar.set(1)
  progressBar.animate(0, {
    duration: data[indicies[nextIndex]].length * 1000
  })

  audio.stfu()
  runStage(window.indicies, nextIndex, 0)
}

var edit = function () {
  sections.runSection.toggleClass('open')
}

buttons.addStage.on('click', addNewStage)
buttons.run.on('click', run)
buttons.pause.on('click', pause)
buttons.resume.on('click', resume)
buttons.stop.on('click', stop)
buttons.next.on('click', next)
buttons.edit.on('click', edit)

stop()

if (loaded) {
  let max_id = 0
  let curr_id
  data = JSON.parse(loaded)

  for (let key in data) {
    curr_id = parseInt(key)
    if (curr_id > max_id) max_id = curr_id
    addStage(curr_id, data[key])
  }
  id = max_id + 1

} else {
  data = {}
  id = 0
}

Notification.requestPermission().then(function(result) {
  notificationsEnabled = result === 'granted'
});